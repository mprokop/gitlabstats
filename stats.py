#!/usr/bin/python
import urllib2, urllib
import json
import requests
import sys
import subprocess
import git
import os

auth_token = $secret
header = {'Authorization': 'Bearer ' + auth_token}
project_no = 0
commit_no = 0
url = 'https://gitlab.com/api/v4/projects?membership=yes&per_page=100&page=1&statistics=true'


def getLoad (url):
    response = requests.get(url,headers=header)
    data = response.json()
    getList (data)
    if response.links.get('next') is not None:
        getLoad (response.links['next']['url'])

def getList (load):
    global project_no
    global commit_no
    for p in load:
        project_no=project_no+1
        print ("Name:"+p['name']+", ID:"+str(p['id'])+", Commits:"+str(p['statistics']['commit_count']))
        commit_no = commit_no + int(p['statistics']['commit_count'])
        #Uncomment in case you need to download all repos for stats purposes
        #
        #os.mkdir("./gitlabstats/git_replica/"+str(p['id']))
        #git.Git("./gitlabstats/git_replica/"+str(p['id'])).clone(p['http_url_to_repo'])


getLoad(url)
print ("========================")
print ("Number of Repositories: " + str(project_no))
print ("Number of Commits: " + str(commit_no))
