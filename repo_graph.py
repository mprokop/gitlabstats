#!/usr/bin/python
import urllib2, urllib
import json
import requests
import sys
import subprocess
import git
import os
import networkx as nx
import pylab as plt

G=nx.Graph()

auth_token = $secret
header = {'Authorization': 'Bearer ' + auth_token}
project_no = 0
commit_no = 0
url = 'https://gitlab.com/api/v4/projects?membership=yes&per_page=100&page=1&statistics=true'


def getLoad (url):
    response = requests.get(url,headers=header)
    data = response.json()
    getList (data)
    if response.links.get('next') is not None:
        getLoad (response.links['next']['url'])

def getList (load):
    global project_no
    global commit_no
    for p in load:
        G.add_edge("2019", str(p['path']))


getLoad(url)

colors = range(G.number_of_edges())
pos = nx.spring_layout(G)
nx.draw(G, pos, node_size=4000, node_color='#A0CBE2', edge_color=colors, linewidths='0',
        width=4, edge_cmap=plt.cm.Blues, with_labels=True)
plt.savefig('gitlab_stats.png')
plt.show() # display
